from cv2 import cv2 #samo import cv2 u mnie nie działa :|
import os
import glob
import numpy as np
import matplotlib.pyplot as plt

pi = np.pi

Obrazki = []
Zrotowane = []
Nobackground = []
ww = [0,1,2,3,3]

# Wczytuję wszytskie obrazki jakie się znajdują w folderze - Obrazy
for picture in glob.glob("./obrazy/*.*"):
     obrazek = cv2.imread(picture,cv2.COLOR_BGR2RGB)
     Obrazki.append(obrazek)

# Ustawiam wagi do threshold
wagi = [90,40,210,240,240]

#Wykonuję cały program dla każdego obrazka
for i in range (0, len(Obrazki)):
    img = Obrazki[i]
    szary = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    ret,thresh2 = cv2.threshold(img,wagi[i],255,cv2.THRESH_BINARY_INV)
    ret,thresh5 = cv2.threshold(img,wagi[i],255,cv2.THRESH_TOZERO_INV)

#Wyliczam kąt theta o który trzeba obrócić każdy obraz, żeby leżał w poziomie
    gray = cv2.cvtColor(thresh5,cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray,50,150,apertureSize = 3)
    lines = cv2.HoughLines(edges,1,np.pi/180,100)
    for rho,theta in lines[0]:
        a = np.cos(theta)
        b = np.sin(theta)

# Ponieważ w wyniku rotacji obrazy czasem znajdują się poza swoimi wcześniejszymi wymiarami, to delikatnie je powiększam
    rows = thresh5.shape[0]+100
    cols = thresh5.shape[1]+280
    

    M = cv2.getRotationMatrix2D((cols/2,rows/2),180*(ww[i]+1)+90+(theta*180)/pi,1)
    efekt = cv2.warpAffine(thresh5,M,(cols,rows))

#Przycinam obrazy do młótków i zmieiam ich rozmiar na jednakowy
    if (i==0): 
        efekt = efekt[0:500, 50:900]
        efekt = cv2.resize(efekt,(270, 100), interpolation = cv2.INTER_CUBIC)
    if (i==1): 
        efekt = efekt[350:1100, 0:2500]
        efekt = cv2.resize(efekt,(270, 100), interpolation = cv2.INTER_CUBIC)
    if (i==2): 
        efekt = efekt[20:150, 230:500]
        efekt = cv2.resize(efekt,(270, 100), interpolation = cv2.INTER_CUBIC)
    if (i==3): 
        efekt = efekt[140:500, 100:1200]
        efekt = cv2.resize(efekt,(270, 100), interpolation = cv2.INTER_CUBIC)
    if (i==4): 
        efekt = efekt[0:700, 100:1300]
        efekt = cv2.resize(efekt,(270, 130), interpolation = cv2.INTER_CUBIC)

# Nie byłem pewny w jaki sposób to wyświetlać, więc wzorowałem się na przykładach
    titles = ['Original','THRESHOLD','No Background','Rotated']
    images = [img, thresh2, thresh5, efekt]
    for j in range (0,4):
        plt.subplot(2,2,j+1),plt.imshow(images[j],cmap='gray')
        plt.title(titles[j])
        plt.xticks([]),plt.yticks([])
    plt.show()
    Nobackground.append(thresh5)
    Zrotowane.append(efekt)

# Wyświetlenie samych efektów
titles = ['Obraz 1','Obraz 2','Obraz 3','Obraz 4','Obraz 5',]
for j in range (0,5):
    plt.subplot(2,3,j+1),plt.imshow(Zrotowane[j],cmap='gray')
    plt.title(titles[j])
    plt.xticks([]),plt.yticks([])



plt.show()

Nobackground[0]=cv2.imwrite('zapis0.png',Nobackground[0])
Nobackground[1]=cv2.imwrite('zapis1.png',Nobackground[1])
Nobackground[2]=cv2.imwrite('zapis2.png',Nobackground[2])
Nobackground[3]=cv2.imwrite('zapis3.png',Nobackground[3])
Nobackground[4]=cv2.imwrite('zapis4.png',Nobackground[4])
