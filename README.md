# README

Ten skrypt Pythona jest przeznaczony do przetwarzania obrazów, przede wszystkim obracania, kadrowania i zmieniania wielkości. Skrypt jest zależny od pakietów `cv2`, `os`, `glob`, `numpy` i `matplotlib.pyplot`.

Oto szczegółowy opis działania skryptu:

## Ładowanie obrazów

Skrypt zaczyna od wczytania wszystkich obrazów z folderu `obrazy`. Obrazy są wczytywane do programu jako RGB za pomocą funkcji `cv2.imread()`.

## Przetwarzanie obrazów

Następnie dla każdego obrazka skrypt wykonuje szereg operacji przetwarzania obrazów. Te operacje obejmują:

1. Konwersję obrazu na skalę szarości za pomocą `cv2.cvtColor()`.
2. Zastosowanie dwóch różnych metod progowania (`cv2.THRESH_BINARY_INV` i `cv2.THRESH_TOZERO_INV`) z różnymi wartościami progu dla każdego obrazu. Wartości progów są zdefiniowane w liście `wagi`.

## Wyliczenie kąta obrotu

Skrypt następnie oblicza kąt, o jaki należy obrócić obraz, aby był poziomy. Robi to za pomocą metody transformacji Hougha. Najpierw wykrywa krawędzie na obrazie za pomocą `cv2.Canny()`, a następnie używa `cv2.HoughLines()` do wykrywania linii na obrazie i obliczania kąta obrotu.

## Obracanie obrazu

Obraz jest następnie obracany o wyliczony kąt. Skrypt najpierw tworzy macierz rotacji za pomocą `cv2.getRotationMatrix2D()`, a następnie stosuje tę macierz do obrazu za pomocą `cv2.warpAffine()`.

## Kadrowanie i zmiana rozmiaru obrazu

Po obróceniu obrazu, skrypt przycina obraz do obszaru zainteresowania i zmienia jego rozmiar na stały rozmiar (270x100 lub 270x130 pikseli). Wielkość i położenie obszaru zainteresowania są różne dla każdego obrazu.

## Wyświetlanie wyników

Na koniec skrypt wyświetla przetworzone obrazy za pomocą `matplotlib.pyplot`. Wyświetla oryginalny obraz, obraz po progowaniu, obraz bez tła i obrócony obraz.

## Zapisywanie wyników

Skrypt zapisuje również przetworzone obrazy bez tła do plików PNG za pomocą `cv2.imwrite()`.

Podsumowując, ten skrypt jest narzędziem do przetwarzania obrazów, które może być użyte do automatycznego obracania, kadrowania i zmiany rozmiaru obrazów.
